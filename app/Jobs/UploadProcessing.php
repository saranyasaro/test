<?php

namespace App\Jobs;
use App\uploads;
use Illuminate\Support\Facades\Validator;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailer;

class UploadProcessing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    Protected $name;
    Protected $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name,$email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $name = $this->name;
        $email = $this->email;
        $path = public_path();
        $arr_flip = [];
        
        if (($handle = fopen($path.'/'.$name, 'r')) !== FALSE) { // Check the resource is valid
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { // Check opening the file is OK!
               $data1[] = $data;
            }
            fclose($handle);
            $headers = $data1[0];
            $data2 = array_slice($data1,1);
            //array flip
            foreach ($data2 as $key => $value) {
                foreach($value as $key1 => $value1) {
                    $arr_flip[$key1][$key] = $value1;
                }
            }

             $val_Arr = $this->checkValidation($arr_flip,$headers);
        }

        if(count($val_Arr) == 0) {
            foreach ($data2 as $key => $value) {
                $upload = new uploads();
                $upload->Module_code = $value[0];
                $upload->Module_term = $value[1];
                $upload->Term_name = $value[2];
                // HERE add more feilds 
                $upload->save();
            }
        }
        else {
            $subject="File upload";
            $mailer->send('email.upload', ['subject' => $subject,'email'=>$email, 'val_Arr'=>$val_Arr],
                function ($m) use ($subject,$email,$val_Arr) {
                    $m->from('saranyasaro74231@gmail.com');
                    $m->to($email)->subject($subject);
                });
            }
    }

    // Function to check validation

    public function checkValidation($arr_flip,$headers){
        $val_Arr = [];
        foreach($arr_flip as $key => $value) {
            $error_flag = false;
            $error_flag1 = false;
            $error_flag2 = false;
            $str = "";
            $str_num = "";
            $str_email = "";
            foreach($value as $key1 => $value1) {
                $error_row = $key1+2;
                $validator_arr = Validator::make(
                    [
                        'cell'      => $value1,
                    ],
                    [
                        'cell'          => 'required',
                    ]
                );
                if ($validator_arr->fails()) {
                    $error_flag = true;
                    $str .= $error_row.",";
                } else {
                    if($key == 0){
                        $validator_arr1 = Validator::make(
                            [
                                'cell'      => $value1,
                            ],
                            [
                                'cell'          => 'numeric',
                            ]
                        );
                        if ($validator_arr1->fails()) {
                            $error_flag1 = true;
                            $str_num .= $error_row.",";
                        }
                    }
                    if($key == 2){
                        $validator_arr2 = Validator::make(
                            [
                                'cell'      => $value1,
                            ],
                            [
                                'cell'          => 'email',
                            ]
                        );
                        if ($validator_arr2->fails()) {
                            $error_flag2 = true;
                            $str_email .= $error_row.",";
                        }

                    }

                    // HERE add more validators for $key = 3,4...
                }
            }
            
            if($error_flag){
                $val_Arr[] = $headers[$key]." missing in row ". $str;
            }
            if($error_flag1){
                $val_Arr[] = $headers[$key]." contains special characters in row ". $str_num;
            }
            if($error_flag2){
                $val_Arr[] = $headers[$key]." contains invalid emailId in row ". $str_email;
            }
        }
        return $val_Arr;
    }
}
