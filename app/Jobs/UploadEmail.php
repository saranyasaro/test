<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailer;

class UploadEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email;
    Protected $val_Arr;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email,$val_Arr)
    {
        $this->email = $email;
        $this->val_Arr = $val_Arr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $email = $this->email;
        $val_Arr = $this->val_Arr;
        $subject="File upload";
        $mailer->send('email.upload', ['subject' => $subject,'email'=>$email, 'val_Arr'=>$val_Arr],
            function ($m) use ($subject,$email,$val_Arr) {
                $m->from('saranyasaro74231@gmail.com');
                $m->to($email)->subject($subject);
            });
    }
}
