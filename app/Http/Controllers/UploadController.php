<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Jobs\UploadProcessing;

class UploadController extends Controller
{
    public function Upload(Request $request)
    {
        $file = $request->file('filecsv');
        if ($request->hasFile('filecsv')) {
            $name = time() . '-' . $request->file('filecsv')->getClientOriginalName();
            $validator = Validator::make(
                [
                    'file'      => $file,
                    'extension' => strtolower($file->getClientOriginalExtension()),
                ],
                [
                    'file'          => 'required',
                    'extension'      => 'required|in:csv',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['status' => "error",'data' => null,"messages" => $validator->errors()], 202);
            }
            $path = file_put_contents(public_path($name),file_get_contents($file));
            $email = 'charush@accubits.com';
            $this->dispatch(new UploadProcessing($name, $email));
            return response()->json(['status' => "success",'data' => null,"messages" => "File upload completed"], 400);
        }
    }
}
